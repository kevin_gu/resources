Project Wisepolis Phase I Scope and Details
====================

This document defines the scope and the some of the details of the IT foundation part of project "wisepolis". 
The content and most of the description is from business perspective and consequently it does not cover the technical designs.
Additional , the content about how development and deployment interactive with business analysis, business operation and marketing operation will be covered in the other document

On this page:


# Table of Contents
1. [Project Summary](#project-summary)
2. [Business Funtions Summary](#business-functions-summary)
3. [Business Flow](#business-flow)
4. [Domain Terminologies](#domain-terminologies)
## project summary
Wisepolis project involves the infrastructure design and development task for a social media native app as the main customer facing product and related deployment and maintenance information technologies task.
## business functions summary
(This article only covers phase 1 details)
#### AuthN/Z
1. Allow user login IOS app with wechat(only) account
1. Allow user logout with single button click action
1. creates and manages wechat enterprise account for wisepolis Team
1. Restrict user's access of persistence to his/her own account and events 
1. Restricted access of viewing confidential information about other hosted events
1. Restricted access of viewing alerts

#### alert/notification management
1. allow users to view alerts.
1. allow users to change alerts settings

#### event hosting
1. Allow user to persist new event info to our cloud service through app and become the host of such event
1. business event will have additional feature such as end time and organization
1. allow host to receive real time update on attendees change
1. allow host to modify limited set of attributes of event
1. allow host to share the event to wechat through circle and chat
1. `TODO: allow host to broadcast notification to attendees`
1. `TODO: allow host to invite(other than wechat share)`


#### event attending 
1. Allow user to explore/search upcoming(`TODO: explore or search? for explore what will be the query look like`)
1. allow user to accept invitation in wechat to become an attendee
1. will receive both in-app and email notification for user's events
1. allow user to take action `TODO: missing ux/ui design here)`
1. `TODO: allow user to accept invitation in-app)`

## operations summary
(This article only covers phase 1 details)
#### deployment
1. monthly(nearly) production/staging deployment
1. active dev deployment and demo support
1. infrastructure risk alerts and support

#### monitor
1. real time monitor for new user creation event
1. real time monitor for new event creation event
1. infrastructure alerts and support


## Business Flow

#### app site map

![Alt text](https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/sitemap.png "Optional title")
> This shows the general flow of app process.
> the detail business flow will be cover later.
> 
> `TODO: missing flow for search and invitation in app`.
>
> `TODO: missing invitaion acceptance process in app`
>
> `TODO: do we need to do in app alert/notification process (at least support one type of alert)`
----

*related ui design:*

[event list][events],
[event detail][events_detail],

[event create page][eventCreate],
[event address][eventCreate_address],
[event options][eventCreate_options],
[event create time][eventCreate_time],
[event title][eventCreate_title],
[event create confirmation][eventCreate_confirmation],


["me" page] [me],

[no access page][noAccess],
[signup page] [signup],

[own event] [ownedEvent],
[edit owned event] [ownedEvent_edit],
[alter media attachement] [ownedEvent_mediaAttachment],

[wecome page 1][welcome1],
[wecome page 2][welcome2],

![Alt text](https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/invitationSiteMap.png "Optional title")
> This shows the general flow of web process.
> the detail business flow will be cover later.
> 

*related ui design:*

[invitation web][invitationWeb],
[invitation web reply] [invitationWeb_reply],
[invitation web reply2] [invitationWeb_reply2],
[invitation web_confirmation] [invitationWeb_confirmation]

#### new user enter and found anonymous RSVP in our record
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/newUser.png "Optional title")
> click access button
>
> > wechat integration
> >
> > send back wechat post login info to cloud
> >
> > cloud services verifies it found it is new
>
> cloud services push back to signup page

*related ui design:*

[no access page][noAccess],
[signup page] [signup],




#### new user enter and found no anonymous RSVP in our database
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/newUser_noAnony.png "Optional title")
> enter email address
>
> > frontend format validation
> 
> press process button
> 
> > backend collision check (email has to be unique in our database)
> >
> > backend anonymous RSVP lookup => not found
>
> welcome page 1

*related ui design:*

[wecome page 1][welcome1],
[signup page] [signup],

#### new user enter and found anonymous RSVP in our database
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/newUser_foundAnony.png "Optional title")
> enter email address
>
> > frontend format validation
> 
> press process button
> 
> > backend collision check (email has to be unique in our database)
> >
> > backend anonymous RSVP lookup => found
>
> cloud service pushed possible events to welcome page 2

*related ui design:*

[wecome page 2][welcome2],
[signup page] [signup],

#### existing user enter
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/oldUser.png "Optional title")
> click access button
>
> > wechat integration
> >
> > send back wechat post login info to cloud
> >
> > cloud services verifies it and found it is old
> >
> > cloud services found event lists for it
> >
>
> cloud services push event list to event page

*related ui design:*

[no access page][noAccess],
[event list] [events],

#### web invitation process
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/webInvitation.png "Optional title")
> click wechat web link
>
> > retrieves website from service with event id in url
> >
> > fill out all the info (email, name, guest, type going/maybe)
> 
> press "join the event"
> 
> > cloud services record the info 
> >
> > cloud services sends confirmation email
> >
> > `TODO: how to handle email send fail? ignore/different page/revert the db change`.
> >
>
> cloud services push event detail to the confirmation page

*related ui design:*

[invitation web][invitationWeb],
[invitation web reply] [invitationWeb_reply],
[invitation web reply2] [invitationWeb_reply2],
[invitation web_confirmation] [invitationWeb_confirmation]

#### event management process
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/eventManagement.png "Optional title")

general attributes:

> select event from event list
>
> > retrieves owned event from cloud
> >
> 
> press "edit" and goes to edit mode
> 
> 
> press "enter" to each field
> 
> 
> press save after editing
> 
> >
> > cloud services record the change 
> >
>
> cloud services response with success or failure

or attachment 

> select event from event list
>
> > retrieves owned event from cloud
> >
> 
> press "attachment" and goes to attachment page
> 
> 
> press pdf or image
> 
> 
> select files and press save
> 
> >
> > cloud services record the change 
> >
>
> cloud services response with success or failure signal

*related ui design:*

[event list][events],
[event details][events_detail],
[own event] [ownedEvent],
[edit owned event] [ownedEvent_edit],
[alter media attachement] [ownedEvent_mediaAttachment],


#### casual event create
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/eventCreateCasual.png "Optional title")
> select any casual event type from 2 major types (casual and business) 
>
> tick the check box of two options (public event and RSVP needed) 
>
> enter event detail and title
> 
> input event address
> 
> >
> > google map api returns suggestion.
> >
>
> display on the event address page
>
>
> pick event start time and press create
>
> >
> > wisepolis cloud services persist
> >
>
> return back to event list page


*related ui design:*

[event create page][eventCreate],
[event address][eventCreate_address],
[event options][eventCreate_options],
[event create time][eventCreate_time],
[event title][eventCreate_title],

#### casual event create
![Alt text](https://gitlab.com/kevin_gu/resources/raw/master/wisepolisPhase1/gliffy/eventCreateBusiness.png "Optional title")
> select any casual event type from 2 major types (casual and business) 
>
> tick the check box of "public event"
>
> enter event detail and title and organization info
> 
> input event address
> 
> >
> > google map api returns suggestion.
> >
>
> display on the event address page
>
>
> pick event start time and end time and press create
>
>
> confirmation page lists all previous info
>
> >
> > wisepolis cloud services persist
> >
>
> return back to event list page

*related ui design:*

[event create page][eventCreate],
[event address][eventCreate_address],
[event options][eventCreate_options],
[event create time][eventCreate_time],
[event title][eventCreate_title],
[event create confirmation][eventCreate_confirmation],


## Domain Terminologies
- - -
Account

name     | sample   | value 
---------:| :----- |:-----:
wisepolisID  |  1 | integer value of id
wechatID     |    afha$73jd | wechat returned ID
wechatDisplay      |     N/A | image artifact
emailAddress      |     flyonthetable@gmail.com | text (validated)
ownedEventList      |     N/A | list of events
attendedEventList      |     N/A | list of events
appNotification      |     true | true/false
emailNotification      |     true | true/false


event

name     | sample   | value 
---------:| :----- |:-----:
eventID  |  1 | integer value of id
type     |    casual | business/casual
subtype      |     N/A | image artifact
starttime      |     flyonthetable@gmail.com | text (validated)
endtime      |     20170601 9pm 30:30 | time
address      |     20170601 9pm 30:30 | time
title      |     party | text
detail      |     year end party...| text
organization      |     BMO | text
owner      |     true | account
attendeeList      |     true | list of account

notification

name     | sample   | value 
---------:| :----- |:-----:
notificationID  |  1 | integer value of id
from     |    2 | event or system
to      |     1 | userID
type      |     attendance | system/eventInvitation/attendance
message      |     there is a new attendee added to your event| text
title      |     new attendance | text
sendTime      |    20170601 9pm 30:30 | time

invitation 

name     | sample   | value 
---------:| :----- |:-----:
1. `TODO: invitation not designed yet`


[invitationWeb]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/invitationWeb.png
[invitationWeb_reply]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/invitationWeb_reply.png
[invitationWeb_reply2]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/invitationWeb_reply2.png
[invitationWeb_confirmation]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/invitationWeb_confirmation.png

[events]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/events.png
[events_detail]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/events_detail.png

[eventCreate]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate.png
[eventCreate_address]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate_address.png
[eventCreate_options]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate_options.png
[eventCreate_time]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate_time.png
[eventCreate_title]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate_title.png
[eventCreate_confirmation]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/eventCreate_confirmation.png

[events]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/events.png
[events_detail]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/events_detail.png

[me]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/me.png

[noAccess]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/noAccess.png
[signup]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/signup.png

[ownedEvent]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/ownedEvent.png
[ownedEvent_edit]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/ownedEvent_edit.png
[ownedEvent_mediaAttachment]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/ownedEvent_mediaAttachment.png

[welcome1]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/welcome1.png
[welcome2]: https://assets.gitlab-static.net/kevin_gu/resources/raw/master/wisepolisPhase1/screenshot/welcome2.png