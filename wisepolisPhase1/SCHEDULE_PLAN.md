Project Wisepolis Phase I Schedule and Plan
====================

This document contains the schedule and planning for phase I and defines how development and deployment interactive with business analysis, business operation and marketing operation will be covered in the other document

On this page:

# Table of Contents
1. [Summary](#summary)
2. [planning](#planing)
    1. [AuthN/Z stream](#authn/z-stream)
    1. [create event stream](#event-create-stream)
    1. [event management stream](#event-management-stream)
    1. [event attend stream](#event-attend-stream)
3. [schedule](#schedule)
3. [interaction within the team](#interaction-within-the-team)
    1. [development and deployment](#development-and-deployment)
    1. [development and business team](#development-and-business-analyst-team)
##  summary
Although the main product of this project is the customer facing native app, the entire infrastructure beneath that is absolutely microservices/cloud orientated designed 
for the future valuable analytic data vision and robust,functionally easy extendable and scale extendable. The development and deployment pipeline of this project (including CI/CD) is, therefore, more agile driven
in phase one, the target deliver time will be set around 3 months that includes development, testing, verification and deployment.

## Planing
(This article only covers phase 1 planning)
#### AuthN/Z stream

- - -

task name    | description  | points 
---------:| :----- |:-----:
entity design  |  design/create account related tables | 5
wechat account administration     |    create wechat enterprise account and generate tokens for our cloud services | 5
account service presentation API      |    development and testing of api that hosts account information which supports pagination | 15
account service persistence API      |    development and testing of account persistence | 10
mobile api security design      |     implement a token base security(authN/Z) for wisepolis app api | 10
native app wechat integration      |  integration with wechat login process in native app | 5
native app login/signup flow      |     signup process | 10
email service integration      |     server end email registry process | 10

#### event create stream

- - -

task name    | description  | points 
---------:| :----- |:-----:
entity design  |  design/create event and event creation related tables | 15
event create process    |    create event creation process in app | 10
event service presentation API      |    development and testing of api that hosts event information which supports pagination | 15
event service persistence API      |    development and testing of event persistence | 10
event retrieve process      |  retrieve single event in native app | 5


#### event management stream

- - -

task name    | description  | points 
---------:| :----- |:-----:
entity design  |  design/create attachment related tables| 5
event detail and edit page    |    native app development and testing for event detail info  | 10
event attachment page    |    native app development and testing for attachment edit process | 5
contentManager service presentation API      |    development and testing of api that supports artifact lookup and retrieve| 15
contentManager service persistence API      |    development and testing of attachment artifact persistence | 10
event service presentation API       |     implement event attribute representation api | 5
event service persistence API      |  implement event attribute edting api | 5


#### event attend stream

- - -

task name    | description  | points 
---------:| :----- |:-----:
entity design  |  design/create anonymous RSVP related tables | 5
website     |    create 4 pages for invitationWeb process | 10
event service presentation API      |    development and testing of api retrieves event detail info | 5
event service persistence API      |    development and testing of anonymous RSVP persistence api | 10
event service persistence API  II    |    development and testing of add event persistence api | 10
add event with code      |    create add event process in app | 5


## Schedule

stream name    | resource  | month/release # 
---------:| :----- |:-----:
AuthN/Z stream  |  3 | 1
event create stream     |   3  | 2
event management stream      |  3   | 2~3
event attend stream      |   2  | 3

## interaction within the team
#### development and deployment
1. 3 environments development, staging and produciton
    1. development is only accessible by developers which runs snapshot code that is under development and testing
    1. once the code has been tested it will move to master and thus deployed to staging, business verification and demo are always using this environment
    1. monthly based (or semi monthly depending on the development process) master code will be synchronized to production
1. release notes will be posted during the production release
#### development and business analyst team
1. weekly sprint update meeting, that talk about what is done, doing and will do
    1. development team will present demo to business for what is done.
    1. feedback will be recorded and impact the upcoming task and agile board
1. technical support and training for release product
    1. within half date support will be given for technical difficulty and support
    1. regular training (according to the delivered feature) will be given to business team and operators
1. document about the administration process will be given at the end of the work package


please refer to [agile] software development cycle. 

[agile]: (https://en.wikipedia.org/wiki/Agile_software_development)