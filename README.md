Project Wisepolis Documentation and design resources
============================================================

This repo contains all the documentation about wisepolis and design resources.
On this page:


# Table of Documents
1. [Project Wisepolis Phase I Scope and Details][wisepolisIScope&Detail]
1. [Project Wisepolis Phase I Schedule and Planning][wisepolisISchedule&Plan]
1. [`TODO:Project Wisepolis Phase I Training documentation`](#functions-summary)

[wisepolisIScope&Detail]: https://gitlab.com/kevin_gu/resources/blob/master/wisepolisPhase1/README.md
[wisepolisISchedule&Plan]: https://gitlab.com/kevin_gu/resources/blob/master/wisepolisPhase1/SCHEDULE_PLAN.md
